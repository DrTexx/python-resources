# Python Resources

Resources to help you learn and use python

## Getting started

- [Learn Python the Hard Way](https://learnpythonthehardway.org/python3/preface.html) (Beginners)

## Virtualenvs

- [Pipenv and Virtual Environments - The Hitchhiker's Guide to Python](https://docs.python-guide.org/dev/virtualenvs/)
